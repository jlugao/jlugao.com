---
title: "Short Guide to FastAPI Part 2 - Proof of Concept"
date: 2020-07-14T22:33:30-03:00
draft: false
---

Hello, today we will begin our link shortener application. We will call it "shostener" after a brazillian cartoon show character. You can find the code for this part of the tutorial [here](https://github.com/blurg/shostner/tree/v0.1). 

A quick warning: If you are just starting out, don't be scared, I won't talk about how to run code, or how to structure it, all I want you to understand is what resources we have to solve our problems and how they will be solved. The app structure, where we are going to store stuff, which files should be created and where, all of this will come later on this series.

The goal of a Proof of Concept is to make sure that what we want to do is feasible, meaning that we won't waste time building the entire application just to realise that the idea doesn't work after all. For our POC we will need to prove that we can redirect requests to URLs that are can be fetched by the application by a key, and that we can log the request data without making the redirect take a lot of time.

To start, We will write our application as follows:

``` python

from fastapi import FastAPI
from starlette.responses import RedirectResponse

app = FastAPI()

urls = {
    "google": "http://www.google.com.br",
    "me": "http://jlugao.com"
}

@app.get("/{shortened_url}")
async def get_url(shortened_url: str):
    return RedirectResponse(url=urls.get(shortened_url, "http://www.facebook.com"))

```

FastAPI is built on top of a framework called [Starlette](https://www.starlette.io/), this framework implements a `RedirectResponse`, which will allow us to redirect anyone that goes to our endpoint to an arbitrary url.
We then make a list of urls, where the keys will be the keywords that we will receive on our url. So if I receive a GET request at the endpoint `/me`, we will redirect the user to my website. And if the server can't find the keyword it will just redirect to the first website I could think of.

Ok, this was simple. Starlette had our back here and provided us the tool to do this easily. Running locally the response time is great, we can continue.

Now we need to see if we can log information from our visitors' requests. For this, we will write the following code:

``` python

from fastapi import FastAPI, Header, Request
from starlette.responses import RedirectResponse
from typing import Optional

app = FastAPI()


urls = {
    "google": "http://www.google.com.br",
    "me": "http://jlugao.com"
}

@app.get("/{shortened_url}")
async def get_url(  shortened_url: str,
                    request: Request,
                    user_agent: Optional[str] = Header(None),
                    accept_language: Optional[str] = Header(None),
                    referer: Optional[str] = Header(None)):

    print(f"Shortened URL: {shortened_url}")
    print(f"Targer URL: {urls.get(shortened_url, 'http://www.facebook.com')}")
    print(f"User Agent: {user_agent}")
    print(f"Referer: {referer}")
    print(f"Ip: {request.client.host}")
    print(f"Language Preferences: {accept_language}")

    return RedirectResponse(url=urls.get(shortened_url, "http://www.facebook.com"))

```

Our function is starting to get a bit more complex, but let's break this down. FastAPI let's you access the request object directly by declaring it as a function variable `request: Request`. But also provides a nice way of acessing the header variables like the user agent to find out what kind of browser our visitor is using by passing `user_agent: Optional[str] = Header(None)` to the function declaration, this says that user_agent will default to None if it is not present on the header, and if present will have the value on the header.

With this we can print to the terminal the keyword, the targe url, the browser being used, the address from which this url was called, the IP address and the language preferences of the user's browser. This will let us extract lots of statistics of our visitors in the future.

This is not perfect, as printing something locally might feel fast, I can Imagine this getting slow on a server having to save this data to a database. Let's see what we can do to move this to the background.


``` python

from fastapi import FastAPI, Header, Request, BackgroundTasks
from starlette.responses import RedirectResponse
from typing import Optional

app = FastAPI()


urls = {
    "google": "http://www.google.com.br",
    "me": "http://jlugao.com"
}


def log_access_info(shortened_url: str, user_agent: str, referer: str, ip: str, language: str):
    print("Just Redirected a request with the following data")
    print(f"Shortened URL: {shortened_url}")
    print(f"Targer URL: {urls.get(shortened_url, 'http://www.facebook.com')}")
    print(f"User Agent: {user_agent}")
    print(f"Referer: {referer}")
    print(f"Ip: {ip}")
    print(f"Language Preferences: {language}")
    print("====================")

@app.get("/{shortened_url}")
async def get_url(  shortened_url: str, 
                    request: Request,  
                    background_tasks: BackgroundTasks,
                    user_agent: Optional[str] = Header(None), 
                    accept_language: Optional[str] = Header(None), 
                    referer: Optional[str] = Header(None)):
    background_tasks.add_task(log_access_info, shortened_url, user_agent, referer, request.client.host, accept_language)
    return RedirectResponse(url=urls.get(shortened_url, "http://www.facebook.com"))

```

Ok, so all we needed to do is import `BackgroundTasks` from `FastAPI` and use it's `add_task` method to call a new function we created that receives all the access info and logs it (in our POC it will just print to the terminal). The way this works is that after returning the response the function will run.

To me this looks enough to prove that we can do this project. On the next parts of this series we will slow down a bit and build our application piece by piece until we have a functional, production-ready personal url shortener. See you on the next post!