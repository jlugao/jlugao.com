---
title: "Short Guide to FastAPI Part 1 - Hello"
date: 2020-07-14T21:58:02-03:00
draft: false
---

Hello, today we will continue to talk about FastAPI, I'll try to give a quick introduction before we get to our application (you can skip ahead if you have already done and understood the hello world at the FastAPI docs)

Before we begin, I'll talk a lot about type annotations, if you don't know what that is: Type annotations are a somewhat new feature in python, it lets you define what type a variable is. This is completelly optional and has no interference with how the code is run, it is just used by code checkers to make sure you didn't mix anything up. Usually they look something like this: `x: int = 1`, `y: str` and so on. The genious of FastAPI is that it takes those optional annotations and makes them usefull for validation and documentation.

FastAPI can be deceivengly simple on hello world applications. If you head to FastAPI documentation page you will find this snippet on the front page:

``` python
from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}

```

This snippet is FastAPI's Hello World. In a few lines it stablishes two endpoints, one basic hello world at the root of the application and one representing a more complex usecase that returns an url parameter as well as the contents of an optional querystring. I think I might be going a bit too fast, so I'll try and break this down a bit.:

If you want to run this locally: start a virtualenv, install FastAPI and uvicorn with `pip install fastapi uvicorn`, save the snippet as `app.py` and run with `uvicorn app:app`, then you can head to `localhost:8000` on your browser and start exploring.

- `app = FastAPI()` instantiates an app (this app object is what will be run by the server)
- creates an endpoint at the root of the application (running locally will be at "localhost:8000") that returns a json with the content `{"hello": "world"}`
- creates an endpoint that recieves an `item_id` parameter, the type annotations in the function is used to validate this value as an integer. This parameter is then returned on the json response.
- as an extra this endpoint accepts an optional querystring, which is a special part of urls used to pass extra parameters to servers, such as but not limited to pagination info, search parameters and filters. The way this is setup you can go to `http://localhost:8000/items/1?q=hello`. The `q: Optional[str] = None` part of the function will tell FastAPI to get the value of the `q` on the querystring, this value is then returned on the json. If you go to the url I just mentioned with a server running this application locally, you will get the response `{"item_id": 1, "q": "hello"}`
- This code can be run using an async server called gunicorn, which is extremelly fast and easy to use

One interesting thing is that all the code snippets found on the FastAPI documentation are actual code, the docs are setup to get code snippets from automated tests. This is one very interesting way of making sure that the docs stay up to date.

If you head over to `localhost:8000/docs` with the server running, you will get to a documentation page. on the read_item endpoint you will see that FastAPI understood the type annotations passed on the function definition. Also, you can try to put a string on the `item_id` and see that you will get a validation error.

This is a lot of functionallity from just a few lines of code.

If you are new to type annotations and you are used to python, you might be thinking "this doesn't even look like python anymore." and you know what? I completelly understand, that's how I felt the first time I saw something like this, but if I can ask you anything is to give it a try, follow this series, write a small application and you will feel how this works. If you are anything like me, in no time you will feel your code editor's autocomplete become a bit more magical with code that you've written, you will catch errors before even running the app and will get used really fast. If after trying you still don't like type annotations, well, this might just not be your cup of tea and that's alright, there are plenty of other frameworks to use out there and if library authors use type annotations right you might get the bennefits without even using it in your own code.

That is it for today and I'll see you on the next post where we will start building our app with a proof of concept.