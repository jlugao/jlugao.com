+++
title = "Sharpening the axe #1 - Typescript"
date = "2019-02-24"
menu = "posts"
draft = true
+++

---

This series is all about sharing what I've learned and shouldn't be viewed as tutorials

---

Today I'll share with you guys that I am starting to learn typescrypt, so I've setup a really simple project structure, which I think will be usefull on this quest. In the future you will see me doing a fizzbuzz in javascript (I do this for every language I learn)

but first, references:

- I used [this book](https://basarat.gitbooks.io/typescript/docs/testing/jest.html) to learn everything applied here
- And also [this medium post](https://medium.com/@equisept/simplest-typescript-with-visual-studio-code-e42843fe437) for setup

Ok, let's get to it

First, install typescript

```
npm install -g typescript
```

change to your project directory and type:

```
tsc --init
```

this will create a tsconfig.json, which will be used to build the js files from the ts files.

Now let's add jest for testing

```
npm i jest @types/jest ts-jest -D
```

also, we must configure jest

```javascript
module.exports = {
  roots: ["<rootDir>/src"],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"]
};
```

change the package.json to

```javascript
{
  "test": "jest",
  "devDependencies": {
    "@types/jest": "^24.0.6",
    "ts-jest": "^24.0.0",
    "typescript": "^3.3.3333"
  }
}
```

last setup step: create a `src` folder

## Alright, enough with the boilerplate

ok, let's create a `src/classes.ts`:

```typescript
export class Point {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  add(point: Point) {
    return new Point(this.x + point.x, this.y + point.y);
  }
}
```

aaaand the tests:

```typescript
import { Point } from "./classes";

test("basic", () => {
  var p1 = new Point(0, 10);
  var p2 = new Point(20, 0);
  var p3 = p1.add(p2);
  expect(p3).toEqual(new Point(20, 10));
});
```

now, if we want to build the .ts files to .js, just hit CMD+SHIFT+B and vscode will take care of that.

and we can do something really cool:

```
npx jest --watchAll
```

this will run our test and watch for changes, if we save any changes it will rerun the test (hopefully just the relevant tests are rerun)

That's all for today, hopefully I'll come back to share more of what I learned and make proper tutorials.
