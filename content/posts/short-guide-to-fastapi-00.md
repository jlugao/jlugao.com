---
title: "Short Guide to Fastapi Introduction"
date: 2020-07-13T22:27:25-03:00
draft: false
---

FastAPI is one of the most exciting technologies I've used so far, there is something incredibly refreshing about it that I can't quite put my finger on. I don't know if it is the magical feeling we get from auto-complete of a codebase filled with type annotations, the amazing docs or the automatic documentation.

Whatever it is, if you are a web developer using python, I recommend that you check out fastapi, it might be a great option for your next project. It is an async framework, leverages pydantic and type annotations, blazing fast and provides great developer experience (ok, this is extremely subjective, but I find using FastAPI great)

The [docs](https://fastapi.tiangolo.com/) are a great place to start, get a feel of what all the fuzz is about. It also has lots and lots of info that will help you from the basic api to database connection and authentication. However I feel like a guided and opinionated tutorial is always the best way to start. This is why I am starting a series on using FastAPI.

In this series we will build a personal url shortener service (in case I want to send url.jlugao.com/link to anyone), I'll make sure to include the following:

- Linting and Formatting
- Connecting a database
- Testing
- Suggested structure for **small** projects

In terms of features we want:

- Automatic url alias generation (for when you just want a quick link shortening)
- Manual url alias (for when you want to customize your links).
- Analytics (We want to log each access and be able to store: IP/country, date/time)
- A dashboard to create urls and see some very simple analytics.

That is it for today and I'll see you on the next post where we will look at how to create a Hello World application with FastAPI.