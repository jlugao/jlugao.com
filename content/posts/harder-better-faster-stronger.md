---
title: "Harder, Better, Faster, Stronger - Coming back to the Blog"
date: 2022-01-28T14:33:30-03:00
draft: false
---

Ok, I have to admit. I let this blog rot for a long time. But no more! Today we resurrect this blog with a different approach.

We are in 2022, if you were in the planet for the last couple of years you know that everything pretty much sucked. And I, as most people, let myself go and got into a rut, I couldn't code properly, couldn't think of personal projects and couldn't even think of studying. I'm not gonna jinx it and say I'm out of the rut already, but I can feel little bursts of energy trying to light the sparkles in my eyes - the ones that fire up whenever I think of an exciting and new project.

To avoid burning out (again) I'm steering my personal projects towards product development, and by that I mean making things we can touch (and the occasional app). I challenged myself with a very though but doable goal: Create 12 products/projects by the end of the year. I have listed some projects I'd like to do, but I believe I'll come up with most at the moment I'm starting them.

There will be a lot of skill building to support those projects, that's why I'm also trying to take 10 courses and read 4 books on the subject during this year. Those are soft goals, as long as I do the projects it's fine, but I believe my life will be much easier creating the products if I know how to do things properly.

Today is January 28th, almost the end of the month, and there is progress already: I have taken the online course [Digital Illustration for Industrial Design](https://www.domestika.org/pt/courses/3207-ilustracao-digital-para-design-industrial) by Andrew Edge on the Domestika platform. This will let me start creating digital concepts in my tablet. I gotta say, the quality of the work I'm able to produce here is bad, but I am a beginner and will keep at it until I can produce something mediocre at least. Forgive me for not sharing some sketches with you, but I think that my self-esteem would suffer a bit if I released something into the world that I'm that insecure about.

I also created a digital finance tracker in google sheets. I'm using this to make some experiments: I've released the Portuguese version for free with a possibility of voluntarily donating to contribute with the author, I'm also going to release the English version on Etsy and see how well it does there. This was a fun project since I already needed to find a better way of tracking my finances.

In case you are curious, here are some of the things that are on my roadmap for this year:
- get my 3d printer back online (I miss you Friday)
- get good at blender for 3d printing
- sketching for product design
- Design Sprint
- Human Centered Design
- Concrete stuff (I mean stuff made out of grout)
- Woodworking

And here are some of the projects I have in mind:
- Chess Board with custom board and pieces
- End-grain cutting board
- Planters for my apartment
- stream deck
- link shortener

I'll try my best to keep you posted and share my progress. It is very possible I'll make redesigning some of the blog a project to make it more suitable for the kind of projects I'm doing now, and also add a Portuguese section, so I can better communicate with my fellow Brazilians.

See you next time,

Behave and be safe.
João