+++
title = "Getting Started With Cpp in 2019- Intro"
date = 2019-09-24T23:03:14-03:00
menu = "posts"
+++

A series about getting started on the right foot when learning modern C++

Getting started with programming languages (when you know other languages) can be easy and hard both at the same time. It is easy, just grab a tutorial and get going, eventually you will understand the basics of the language and what is similar to the language you are coming from and what isn't. It is hard because the amount of information out there today can be overwhelming, plus some tutorials are outdated, most aren't focused on production-like code and you have to pick piece by piece in many tutorials and come up with a mental map of what is like to work in that language.

Well, recently I was presented a challenge to work in a C++ project and guide the team towards best practices. At first it was a bit scary but I managed to break down into manageable pieces:

- Basics
- Modern C++
- Build
- Best Practices
- Standards
- Memory and Pointers
- Testing
- Code Quality
- Extra: Patterns

In this series I intend to guide the readers through insights I had while learning C++ and provide a path to get up and running with C++ today (well, you will probably need to put a lot of study after this to catch up, but this will get you going)

Tl;Dr of the series - for those in a hurry

- Use CMake
- Use a coding standard (IsoC++ standard preferably)
- Use Catch and Trompeloeill or GTest and GMock for testing and mocking
- Use dependency injection to make your classes testable with mocks
- Don't give a f$#%$# about test-induced design issues
- Learn a bit about patterns but don't get neurotic about it
- Avoid allocating memory by hand
- Use smart pointers wherever you can
- Concurrency and memory management are hard and you should respect them (learn the standards that will avoid that you f#@$@ this up)
- Use CLang linters, checkers and everything you can
- Be comfortable compiling from the Cli then with CMake then go to IDEs
- optional: choose a package manager
- Personal note: use CLion as an IDE

Ok, That's about it for today. On the next post we will get started on the first steps, I'll provide my approach on learning a new language and give you resources to follow.