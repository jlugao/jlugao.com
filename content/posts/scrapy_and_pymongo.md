+++
title = "Scrapy and PyMongo"
date = "2019-02-25"
menu = "posts"
+++


Today I tried my luck with a simple spider made in scrapy, since everything went well I persisted the data in a MongoDB collection using pyMongo. Let me walk you through how it was done.

## Install dependencies

(virtualenv is implied)

```
pip install pymongo scrapy
```

## Start Project

```
scrapy startproject example
```

this will give you lot's of boilerplate code. Just cd into the example folder.

ok, the first thing to do now is under the spiders folder, create a file. I called mine `quotes_spyder.py` (yes, just like the official scrapy tutorial - but what can I say, it was the reference I followed, over the next few days I'll try and make something a bit more exciting)

Here I built the following spider:

```py
import scrapy
class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = ["http://quotes.toscrape.com/"]

    def parse(self, response):
        for quote in response.css(".quote"):
            yield {
                "text": quote.css(".text ::text").get(),
                "author": quote.css(".author ::text").get(),
                "tags": quote.css(".tag ::text").getall(),
                "author_url": quote.css(".author + a").attrib['href'],
            }
        for next_page in response.css(".next > a"):
            yield response.follow(next_page, self.parse)
```

this was surprisingly straightforward, a few things to note:

- to run this you type `scrapy crawl quotes` on the terminal. Scrapy will look for the **name** declared in your class
- I think that using css is not currently the most recommended way, apparently xpath can be more precise
- `yield response.follow(next_page, self.parse)` is a really neat shortcut to adding new urls to be scraped

good, now if we run `scrapy crawl quotes` a huge debug dump is going to show up on our terminal and if everything went right somewhere in the middle our results should be displaying.

## Persisting Data

Nice but what do we do with this data? I have a hunch that at this step we just want to gather data and not process it on the fly. With this in mind we should persist the data somewhere so we can fetch it later and do something with it.

I've been wanting to play with MongoDB for a while now. If you don't already know what mongo is, mongo is a NOSQl database which rely on JSON documents to store data. Put simply we can create collections which is a list of arbitrary (I think) json.

we already installed pymongo, so now all we need to do is create a pipeline, which will process our items, it also has a nice method to setup the pipeline and clean it after it ran.

pipelines.py

```py
from pymongo import MongoClient


class MongoExportPipeline:
    """ Adds everything to a mongodb collection """

    def open_spider(self, spider):
        self.client = MongoClient()
        self.db = self.client["test-database"]
        self.quote_collection = self.db["quote_collection"]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        key = {"text": item["text"]}
        self.quote_collection.update(key, item, upsert=True)
        return item
```

I didn't see many secrets here, but somethings to note:

- perhaps if you are running this on a server you might want to store the database info in environment variables
- Instead of using insert_one I chose to use update with the kwarg upsert set to True, this will update the info if the text is the same or insert a new one if this is the first time this text arrived.

One last detail:
at settings.py

```py
ITEM_PIPELINES = {"example.pipelines.MongoExportPipeline": 500}
```

this will tell to use our new pipeline with priority 500 (less is better).

Ok, now we can run our spider again with `scrapy crawl quotes`. If everything went well you should be able to inspect your collection with some mongo gui tool (I am using a vscode plugin called Mongo Runner), you should see the result:

```json
[
  {
    "_id": "5c7489f86bef4b34519a9ab6",
    "text": "“The world as we have created it is a process of our thinking. It cannot be changed without changing our thinking.”",
    "author": "Albert Einstein",
    "tags": ["change", "deep-thoughts", "thinking", "world"],
    "author_url": "/author/Albert-Einstein"
  },
  {
    "_id": "5c7489f86bef4b34519a9ab8",
    "text": "“It is our choices, Harry, that show what we truly are, far more than our abilities.”",
    "author": "J.K. Rowling",
    "tags": ["abilities", "choices"],
    "author_url": "/author/J-K-Rowling"
  },
  {
    "_id": "5c7489f86bef4b34519a9abb",
    "text": "“There are only two ways to live your life. One is as though nothing is a miracle. The other is as though everything is a miracle.”",
    "author": "Albert Einstein",
    "tags": ["inspirational", "life", "live", "miracle", "miracles"],
    "author_url": "/author/Albert-Einstein"
  },
  ...
]
```

I ran the spider once, counted all the records and it amounted to 100, running again I still got 100, this means that the update trick worked. You can also make queries and find specific info like getting (or counting) quotes by author, tags and more.

Hope you guys liked it. Until next time.
