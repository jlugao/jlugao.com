+++
title = "Getting Started With Cpp in 2019- Intermission"
date = 2019-09-25T09:50:00-03:00
menu = "posts"
+++

Ok, monty python joke aside, writing the series on C++ I felt the need to give some more context and explain a bit more about why I learned C++, so I gathered here the answer (and story behind) the two most important questions friends asked me about the last post.

## Why did I decide to learn C++, do I work with C++?

I work closely with a customer that develops an embedded product (sorry, can't really post much more info on the product), my company does the web side of things. We communicate with the devices through https, provide a dashboard and expose a rest api (pretty straight forward stuff). Their dev team had recently gone from one person developing prototypes in hero mode to 3 people trying to understand the codebase and deliver code to production in no time.

I want to point out that I deeply admire the job done by their team, when the lead engineer joined the company he was alone and inherited a project that was already 2 years behind of schedule and managed to create a working prototype quickly that by managerial decisions became the product. Had another developer been hired the company would have closed the doors. The downside of this story is that the managers lost thrust in developers due to past experience (who can blame them?) and didn't understand the need to evolve the product and work in code quality, so they started selling and selling custom functionality. Fast forward 2 years and I found:

- Lack of standard way of compiling the project (you install Qt creator copy the settings from another developer and things should work)
- Development of the same product being made in many repositories
- Lack of tests
- Lack of tooling to help developers write better code
- Lack of a single standard across the team
- Developers frustrated by how tightly coupled everything was in the application (you mess with motors and the screen crashes, ugh... frustrating)

While they were doing the best to keep the boat floating, they were also setting the ship to sink in the future. They were frustrated and knew that the code should be better, people were getting aggressive, defensive and the mood was just plainly bad.

One day the company owner calls me and invites me to visit and tell him what I think of the company and how it could improve, also he was really worried that the partner who actually runs the company had to step down for a month due to personal problems. So, I went there, failed to compile the project on my computer, listened to everyone on the team and got really overwhelmed, I couldn't understand how they could get anything done in that environment. So, I went back to the hotel, got a nice night of not being able to sleep and came up with a plan to help them (and help me to get some sleep).

The next day the company owner took me to lunch and we talked, I expressed my concerns and how I thought that they could start improving (I was giving away my plan for free just so I could sleep at night), he was worried but optimistic and asked me to further develop this plan and help the team implement it.

Long story short I had to start learning C++ in no time, so I read everything I could, watched online classes and started to map everything in my mind (the series is a consolidation of the knowledge gathered, it should help you get started and put you on track to success in C++, however it is not a comprehensive guide of how C++ should be used in the industry) made a document with the practices that should be followed, talked to their lead engineer and started working with the team to help them code better. We worked intensely for a month, managed to get the project out of the danger zone but it still has a lot of room for improvement.

So, lastly: do I work with C++? Yes and No, I help this project with design decisions and occasionally do some code review (at the beginning I reviewed every merge request and then started passing this responsibility to other members of the team). However, the way I structured the knowledge I consider that C++ is now on my toolbelt to be used when needed, it won't be smooth, I'll lack fluency, but I am certain I have proficiency enough to write good quality simple production code (more complex stuff can be solved with more study, experience and probably learning libraries and what not).

## Should you learn C++?

This is entirely up to you. If you want to work with embedded linux systems it is a must, there are also many other fields that heavily require C++ like audio processing, high performance scientific computing and many others. It is a fast and reliable language (that has its downsides like any language) and when used according to the modern standards is pretty much safe. C++ won't go anywhere in the near future, I've been hearing some news about companies using it with webassembly to deliver webapps that require more processing power.

But ultimately learning C++ will make you a better developer, especially if you work with higher level languages such as python, javascript, ruby and others. You have no idea how much more I appreciate python's testing ecosystem and ease of starting to work on a project. Learning a lower level language (C, Rust and others are also valid) will help you get a grasp of what happens with the memory and will force you to face more data structures and design patterns.