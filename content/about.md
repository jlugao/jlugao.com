+++
title = "About"
date = "2019-01-01"
menu = "main"
+++

Hi,

Welcome to my personal blog, here I'll share my notes on coding, electronics, making stuff, life, the universe and everything else.


I don't have much to add to this section yet, but as I go along, please bug me so I add more to this page.

Some quick bullets about me:

- I have a master in electrical engineering.
- Currently working as a software developer at my own company where we make mobile and web apps for our customers.
- I am mostly interested in the prototyping/mvp building process, getting stuff out of the drawing board and bringing them to real life is amazing.
- I tinker with electronics, 3d printing and other maker stuff at my free time.



Thanks for reading!